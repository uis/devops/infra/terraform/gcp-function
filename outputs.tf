output "function" {
  value = google_cloudfunctions_function.function

  description = "google_cloudfunctions_function resource for the Cloud Function created."
}

output "service_account" {
  value = google_service_account.function_invoker

  description = <<-EOT
    google_service_account resource for the Service Account which the function runs as.
  EOT
}
