# Changelog

## [2.2.5](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.2.4...2.2.5) (2025-02-20)

## [2.2.4](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.2.3...2.2.4) (2024-11-29)

## [2.2.3](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.2.2...2.2.3) (2024-11-14)

## [2.2.2](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.2.1...2.2.2) (2024-11-14)

## [2.2.1](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.2.0...2.2.1) (2024-11-14)

## [2.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/compare/2.1.0...2.2.0) (2024-08-29)


### Features

* enable uniform bucket level access for source bucket ([5c91aee](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-function/commit/5c91aee475aebf69feae64ce8b5d49a5bba40aa7))

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.0] - 2024-02-12

### Added

- New variable `source_bucket_force_destroy` allows the source code bucket to be
  forcibly destroyed if required.

## [2.0.4] - 2024-02-02

### Added

- Variable `enable_versioning` added. If set to `true`, versioning is fully enabled for `function_storage` bucket.
  By default, segment versioning is now enabled. To retain the old behavior, explicitly set this parameter to `false`.
- Variable `keep_versions` added. The number of versions to keep if `enable_versioning` is set to `true`. Default is 1.

## [2.0.3] - 2024-01-25

### Added

- Variable `local_files_dir` is now has `null` as a default value. Instead, logic added in main.tf to detect
  if the terraform is running inside a logan docker container or not.

## [2.0.2] - 2023-12-11

### Fixed

- Ensure `event_trigger` logic only creates the dynamic block when `var.event_trigger.event_type != null`.

## [2.0.1] - 2023-11-02

### Added

* Added support to publish to GitLab Terraform registry when tagged using semver

## [2.0.0] - 2023-05-09

### Changed

- Add ability to create event driven functions.

### Fixed

- Add missing `project_id` attribute to `google_project` data source.
- Set `trigger_http` variable default to `null` to allow optional use of new `event_trigger` variable.

## [1.0.2] - 2022-01-13

### Changed

- Changed default for max_instances to the recommended 3000.

## [1.0.1] - 2021-07-16

### Changed

- Added support to Cloud Function egress settings configuration.

### Fixed

- Make sure the created resources get distinct names when the module is called
  more than once.

## [1.0.0] - 2021-04-07

### Added

- Initial version
