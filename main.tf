# kics-scan disable=d6cabc3a-d57e-48c2-b341-bf3dd4f4a120
# The first line in this file is to suppress "Cloud storage bucket should have logging enabled" in kics report.

# The google_project data source is used to get the default projects for the
# Google providers.
data "google_project" "provider_project" {
  project_id = var.project
}

locals {
  # Allow the project variable to override the provider project.
  project   = coalesce(var.project, data.google_project.provider_project.project_id)
  is_logan  = fileexists(".logan.yaml") ? true : false
  files_dir = local.is_logan ? coalesce(var.local_files_dir, "/teraform-data/functions/") : coalesce(var.local_files_dir, "${path.root}/builds")
}

# The source code of the script.
data "archive_file" "function_archive" {
  type = "zip"

  output_path = "${local.files_dir}/${var.name}-source.zip"

  dynamic "source" {
    for_each = var.source_files
    content {
      filename = source.key
      content  = source.value
    }
  }

}

resource "random_id" "source_bucket_name" {
  byte_length = 4
  prefix      = "${var.name}-function-source-"
}

# We do not need the hassle of customer managed keys here.
#trivy:ignore:AVD-GCP-0066
resource "google_storage_bucket" "function_storage" {
  project       = local.project
  name          = random_id.source_bucket_name.hex
  location      = var.region
  force_destroy = var.source_bucket_force_destroy

  uniform_bucket_level_access = true

  versioning {
    enabled = var.enable_versioning
  }

  dynamic "lifecycle_rule" {
    for_each = var.enable_versioning == true ? [1] : []
    content {
      condition {
        age                = 1
        with_state         = "ANY"
        num_newer_versions = var.keep_versions
      }
      action {
        type = "Delete"
      }
    }
  }
}

resource "google_storage_bucket_object" "function_object" {
  name   = "source-${data.archive_file.function_archive.output_base64sha256}.zip"
  bucket = google_storage_bucket.function_storage.name
  source = data.archive_file.function_archive.output_path
}

resource "random_id" "function_invoker_sa" {
  byte_length = 2
  prefix      = "${var.name}-sa-"
}

# A service account which is used to run the script within a Cloud Function.
resource "google_service_account" "function_invoker" {
  project      = local.project
  account_id   = random_id.function_invoker_sa.hex
  display_name = "${var.name} function identity"
}

# Cloud Function name
resource "random_id" "function_name" {
  byte_length = 2
  prefix      = "${var.name}-"
}

# The script itself as a Cloud Function.
resource "google_cloudfunctions_function" "function" {
  project = local.project
  region  = var.region

  name        = random_id.function_name.hex
  description = var.description
  runtime     = var.runtime

  timeout     = var.timeout
  entry_point = var.entry_point

  service_account_email = google_service_account.function_invoker.email

  source_archive_bucket = google_storage_bucket.function_storage.name
  source_archive_object = google_storage_bucket_object.function_object.name

  trigger_http = var.trigger_http

  dynamic "event_trigger" {
    for_each = var.event_trigger.event_type == null ? [] : [1]

    content {
      event_type = var.event_trigger.event_type
      resource   = var.event_trigger.resource

      failure_policy {
        retry = var.event_trigger.retry_on_failure == null ? false : var.event_trigger.retry_on_failure
      }
    }
  }

  ingress_settings = var.ingress_settings

  vpc_connector                 = var.egress_connector
  vpc_connector_egress_settings = var.egress_connector_settings

  available_memory_mb = var.available_memory_mb
  max_instances       = var.max_instances

  environment_variables = var.environment_variables
}

# Allow the service account to invoke the function.
resource "google_cloudfunctions_function_iam_member" "function_invoker_permission" {
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${google_service_account.function_invoker.email}"
}

# Conditionally allow everyone to invoke the function
resource "google_cloudfunctions_function_iam_member" "function_all_invoker" {
  count          = var.allow_unauthenticated_invocations ? 1 : 0
  project        = google_cloudfunctions_function.function.project
  region         = google_cloudfunctions_function.function.region
  cloud_function = google_cloudfunctions_function.function.name
  role           = "roles/cloudfunctions.invoker"
  member         = "allUsers"
}
