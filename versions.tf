# versions.tf specifies minimum versions for providers and terraform.

terraform {
  # Specify the required providers, their version restrictions and where to get
  # them.
  required_providers {
    archive = {
      source  = "hashicorp/archive"
      version = ">= 2.2"
    }
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 3.1"
    }
  }

  required_version = ">= 1.3"
}
