
variable "name" {
  description = "The name of the function."
  type        = string
}

variable "description" {
  description = "A description of what the function does."
  type        = string
}

variable "project" {
  description = "Project to create resources in. Defaults to provider project."
  type        = string
  default     = null
}

variable "region" {
  description = "Region to create resources in. Defaults to London, UK."
  type        = string
  default     = "europe-west2"
}

variable "runtime" {
  description = <<-EOT
    The runtime of this function. Available values can be found here:
    https://cloud.google.com/functions/docs/runtime-support.
  EOT
  type        = string
}

variable "source_files" {
  description = <<-EOT
    The source files for this function, in the form of file_name => file_content.
    This should include any dependency maps, such as requirements.txt or package.json.
  EOT
  type        = map(any)
}

variable "entry_point" {
  description = "Entrypoint to script."
  type        = string
  default     = "main"
}

variable "trigger_http" {
  description = "Whether this function shall be triggerable via HTTP. Cannot be used with event_trigger."
  type        = bool
  default     = null
}

variable "event_trigger" {
  description = "Specify an event trigger for the function. Cannot be used with trigger_http."
  type = object({
    event_type       = string
    resource         = string
    retry_on_failure = optional(bool)
  })
  default = {
    event_type = null
    resource   = null
  }
}

variable "ingress_settings" {
  description = <<-EOT
    Where to allow ingress traffic from, see:
    https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#ingress_settings
  EOT
  type        = string
  default     = "ALLOW_ALL"
}

variable "egress_connector" {
  type        = string
  description = <<-EOT
    The VPC Connector that egress traffic is routed through. See:
    https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#vpc_connector
EOT
  default     = ""
}

variable "egress_connector_settings" {
  type        = string
  description = <<-EOT
    Which egress traffic should be routed through the VPC connector. See:
    https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#vpc_connector_egress_settings
  EOT
  default     = null
}

variable "allow_unauthenticated_invocations" {
  description = "Whether this function can be invoked by unauthenticated users."
  type        = bool
  default     = false
}

variable "timeout" {
  description = <<-EOT
    Maximum time, in seconds, that a script can take to execute. Invocations
    which take longer than this fail.
  EOT
  type        = number
  default     = 120
}

variable "available_memory_mb" {
  description = "Maxiumum memory available to the script in MiB."
  type        = number
  default     = 128
}

variable "max_instances" {
  # This is the recommended default(!)
  # https://cloud.google.com/functions/docs/configuring/max-instances
  description = <<-EOI
    The maximum number of instances that may be run at any one time.
    Default: 3000
  EOI
  type        = number
  default     = 3000
}

variable "environment_variables" {
  description = "Map of environment variables to pass to the function."
  type        = map(string)
  default     = {}
}

variable "local_files_dir" {
  description = <<-EOT
    A local directory where files may be created which persist between runs but
    which are not checked into source control.
  EOT
  type        = string
  default     = null
}

variable "enable_versioning" {
  description = <<-EOT
    The bucket's versioning configuration.
    While set to true, versioning is fully enabled for the bucket.
  EOT
  type        = bool
  default     = true
}

variable "keep_versions" {
  description = <<-EOT
    The number of file versions to keep if enable_versioning is set to true.
    Default: 1
  EOT
  type        = number
  default     = 1
}

variable "source_bucket_force_destroy" {
  description = <<-EOI
    Allow the function source code bucket to be forcibly destroyed, even if the bucket still contains objects.
  EOI
  type        = bool
  default     = false
}
