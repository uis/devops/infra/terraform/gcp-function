# Known Issues

## Function archive directory empty when applying changes in CI runners

**Status:** Active

**Summary:** The `function_archive` file is missing during the CI apply job when `local_files_dir`
is not passed. The archive file is generated at *plan* time and needs copied between the *plan*
and *apply* job.

### Symptoms and Errors

A user is able to apply changes by running logan locally but, the Terraform apply CI job fails to
apply changes stating the `function_archive` is empty raising an error similar to:

```
module.wpmapp.module.uptime_monitoring["webapp"].module.uptime_check_auth_proxy[0].google_storage_bucket_object.function_object: Creating...

Error: open /teraform-data/functions//wpmapp-uptime-source.zip: no such file or directory

  with module.wpmapp.module.uptime_monitoring["webapp"].module.uptime_check_auth_proxy[0].google_storage_bucket_object.function_object,
  on /builds/uis/devops/digital-admissions/admissions-portal/ap-deployment/terraform_data/modules/wpmapp.uptime_monitoring.uptime_check_auth_proxy/main.tf line 67, in resource "google_storage_bucket_object" "function_object":
  67: resource "google_storage_bucket_object" "function_object" {

```

### Explanation

In order to create the proxy function, we need to upload the source of the function to a bucket.
This is generated as a zip file locally which is a limitation of the [current terraform zip file
provider](https://registry.terraform.io/providers/hashicorp/archive/latest/docs/data-sources/file)
in that it can't generate a zip file as an attribute, only as a file on disk.

When running locally, this isn't a problem. If no local files directory is provided, the file ends
up being stored in the volume created by logan for storing terraform data and all is well.

When running in CI we hit a problem: we generate the plan in one CI job and then at a *later date* we
apply the plan. The zip file is generated at *plan* time by the plan job. The zip file isn't there
at *apply* time when we're running in a completely different job.

As something of a hack, there is a [special
variable](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/terraform-pipeline-base.yml?ref_type=heads#L22)
set by the CI jobs, `local_files_dir`, which is a directory which the CI jobs arrange to be
*copied* between plan and apply jobs. (The absolute location of this directory is runner-specific
so we easily can't add it as a default.)

So, when running in CI, you need to ensure that all files generated at plan time are in that
directory to be able to apply the plan.

### Solution

- Add a `local_files_dir` variable like [an example taken from the GitLab bots
  deployment](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gitlab-bots-deployment/-/blob/main/variables.tf?ref_type=heads).
- Ensure that that variable's value ends up being passed to the `local_files_dir` variable of this
  module.
- Ensure you are using the CI pipeline template at a version of
  [`4.3.0`](https://gitlab.developers.cam.ac.uk/uis/devops/continuous-delivery/ci-templates/-/blob/master/CHANGELOG.md?ref_type=heads#430---2024-06-04)
  or later or that the CI job is setting the `local_files_dir` variable.

### Useful Link

https://gitlab.developers.cam.ac.uk/uis/devops/digital-admissions/admissions-portal/ap-deployment/-/issues/242#note_748786
