# Google Cloud Function

This Terraform module allows the creation of a Google Cloud Function from minimal configuration.
This module is best used for small scripts with static configuration, such as uptime checks.

> **IMPORTANT** Google has launched Cloud Run Functions which provide [numerous
> benefits](https://cloud.google.com/functions/1stgendocs/concepts/version-comparison) over
> first generation Cloud Functions. If you are starting a new deployment, prefer using our [Cloud
> Run Function
> module](https://gitlab.developers.cam.ac.uk/uis/devops/infra/terraform/gcp-cloud-run-function).

## Versioning

The `master` branch contains the tip of development and corresponds to the `v1`
branch. Each release is tagged with its bare version number.

## Required APIs and services

The following APIs must be enabled to use this module:

* `cloudfunctions.googleapis.com`

## Implementation

The module wraps the standard
[GCP Terraform configuration for Cloud Functions](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function),
providing sensible defaults for the bucket the cloud function is stored in and the service
account used to run the function. The majority of the configuration available on a Cloud Function
can be customized through variables, but where possible sensible defaults have been applied.

## Example

The following is an example of a basic Python script which can be deployed as a Cloud Function:

```tf
module "hello-world" {
  source = "..."

  # Name used to form resource names and human readable description.
  name        = "hello-world-function"
  description = "A function that logs hello world"

  # Project and region to create resources in - project will default to the provider's project.
  project = "..."
  region  = "europe-west2"

  # A directory which can be used to store local files which are not checked in
  # to source control. Default value is null and if no path provided, the "./builds" local
  # directory will be used for non-logan environment. In case of logan, "/terraform_data/functions"
  # will be used if no alternative path provided.
  local_files_dir = "/terraform_data/local"

  # Whether to allow the function to be invoked without authentication.
  allow_unauthenticated_invocations = true

  # The runtime of the function
  runtime = "python38"

  # The source files of the function, as a map of file name => file content
  source_files = {
    "requirements.txt" = <<-EOT
        requests==2.25.1
    EOT
    "main.py" = <<-EOL
      import logging

      LOG = logging.getLogger()
      logging.basicConfig(level=logging.INFO)

      def main(request):
          LOG.info('Hello world')
          return '{ "status": "ok" }', {'Content-Type': 'application/json'}
    EOL
  }

  # How long to allow the function to run before killing it.
  timeout     = 30

  # A map of environment variables to pass into the function.
  environment_variables = {
    DEBUG = "TRUE"
  }
}
```

See the [variables.tf](variables.tf) file for all variables.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | >= 2.2 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0 |
| <a name="requirement_random"></a> [random](#requirement\_random) | >= 3.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_unauthenticated_invocations"></a> [allow\_unauthenticated\_invocations](#input\_allow\_unauthenticated\_invocations) | Whether this function can be invoked by unauthenticated users. | `bool` | `false` | no |
| <a name="input_available_memory_mb"></a> [available\_memory\_mb](#input\_available\_memory\_mb) | Maxiumum memory available to the script in MiB. | `number` | `128` | no |
| <a name="input_description"></a> [description](#input\_description) | A description of what the function does. | `string` | n/a | yes |
| <a name="input_egress_connector"></a> [egress\_connector](#input\_egress\_connector) | The VPC Connector that egress traffic is routed through. See:<br>https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#vpc_connector | `string` | `""` | no |
| <a name="input_egress_connector_settings"></a> [egress\_connector\_settings](#input\_egress\_connector\_settings) | Which egress traffic should be routed through the VPC connector. See:<br>https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#vpc_connector_egress_settings | `string` | `null` | no |
| <a name="input_enable_versioning"></a> [enable\_versioning](#input\_enable\_versioning) | The bucket's versioning configuration.<br>While set to true, versioning is fully enabled for the bucket. | `bool` | `true` | no |
| <a name="input_entry_point"></a> [entry\_point](#input\_entry\_point) | Entrypoint to script. | `string` | `"main"` | no |
| <a name="input_environment_variables"></a> [environment\_variables](#input\_environment\_variables) | Map of environment variables to pass to the function. | `map(string)` | `{}` | no |
| <a name="input_event_trigger"></a> [event\_trigger](#input\_event\_trigger) | Specify an event trigger for the function. Cannot be used with trigger\_http. | <pre>object({<br>    event_type       = string<br>    resource         = string<br>    retry_on_failure = optional(bool)<br>  })</pre> | <pre>{<br>  "event_type": null,<br>  "resource": null<br>}</pre> | no |
| <a name="input_ingress_settings"></a> [ingress\_settings](#input\_ingress\_settings) | Where to allow ingress traffic from, see:<br>https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/cloudfunctions_function#ingress_settings | `string` | `"ALLOW_ALL"` | no |
| <a name="input_keep_versions"></a> [keep\_versions](#input\_keep\_versions) | The number of file versions to keep if enable\_versioning is set to true.<br>Default: 1 | `number` | `1` | no |
| <a name="input_local_files_dir"></a> [local\_files\_dir](#input\_local\_files\_dir) | A local directory where files may be created which persist between runs but<br>which are not checked into source control. | `string` | `null` | no |
| <a name="input_max_instances"></a> [max\_instances](#input\_max\_instances) | The maximum number of instances that may be run at any one time.<br>Default: 3000 | `number` | `3000` | no |
| <a name="input_name"></a> [name](#input\_name) | The name of the function. | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Project to create resources in. Defaults to provider project. | `string` | `null` | no |
| <a name="input_region"></a> [region](#input\_region) | Region to create resources in. Defaults to London, UK. | `string` | `"europe-west2"` | no |
| <a name="input_runtime"></a> [runtime](#input\_runtime) | The runtime of this function. Available values can be found here:<br>https://cloud.google.com/functions/docs/runtime-support. | `string` | n/a | yes |
| <a name="input_source_bucket_force_destroy"></a> [source\_bucket\_force\_destroy](#input\_source\_bucket\_force\_destroy) | Allow the function source code bucket to be forcibly destroyed, even if the bucket still contains objects. | `bool` | `false` | no |
| <a name="input_source_files"></a> [source\_files](#input\_source\_files) | The source files for this function, in the form of file\_name => file\_content.<br>This should include any dependency maps, such as requirements.txt or package.json. | `map(any)` | n/a | yes |
| <a name="input_timeout"></a> [timeout](#input\_timeout) | Maximum time, in seconds, that a script can take to execute. Invocations<br>which take longer than this fail. | `number` | `120` | no |
| <a name="input_trigger_http"></a> [trigger\_http](#input\_trigger\_http) | Whether this function shall be triggerable via HTTP. Cannot be used with event\_trigger. | `bool` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_function"></a> [function](#output\_function) | google\_cloudfunctions\_function resource for the Cloud Function created. |
| <a name="output_service_account"></a> [service\_account](#output\_service\_account) | google\_service\_account resource for the Service Account which the function runs as. |
<!-- END_TF_DOCS -->
